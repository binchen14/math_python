
from basegui import *

class  SortIntegers(AbstractProblem):
 
    def __init__(self,root,ft):

        self.anws = [ StringVar() for i in range(6) ]

        AbstractProblem.__init__(self,root,ft)

        for i in range(5):
            ttk.Label(self.mp.f2, text = " < ", font=self.ft).grid(row=1,column=2*i+1,sticky="news")
        for i in range(6):
            ttk.Entry(self.mp.f2,textvariable=self.anws[i],width=6,
               font=self.ft).grid(row=1,column=2*i,padx=5,sticky="news")

        self.refresh()

    def refresh(self):

        self.pos =  np.random.randint(0,100,3)
        self.neg = -np.random.randint(1,100,3)
        
        self.tot =  np.concatenate((self.pos,self.neg))       
        np.random.shuffle(self.tot)     # in place shuffle
        self.srt =  np.sort(self.tot)   # increase order
        
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "请将下面的整数（有正有负）由小至大排序？\n",'n')
        self.mp.t1.insert("end", "%d, %d, %d, %d, %d, %d \n" % (self.tot[0],self.tot[1],self.tot[2],
                                      self.tot[3], self.tot[4], self.tot[5]),'n')

        [self.anws[i].set("") for i in range(6)]

    def grade(self):

        ans = [ self.anws[i].get().replace(" ","")  for i in range(6)]
        ans_tot =  ''.join(ans)
        ans_true = ''.join(  [ str(self.srt[i]) for i in range(6) ] )
        ans_opt  = '<'.join( [ str(self.srt[i]) for i in range(6) ] )
        if ans_tot == ans_true :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % ans_opt 

        return [flag, message]

#  |a-b|
class  AbsSub(AbstractProblem):
 
    def __init__(self,root,ft):

        self.anws = [ StringVar() for i in range(2) ]
        self.var1 = StringVar()
        self.var2 = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2, textvariable = self.var1, font=self.ft).grid(row=1,column=0,sticky="news")
        ttk.Label(self.mp.f2, textvariable = self.var2, font=self.ft).grid(row=1,column=2,sticky="news")
        ttk.Label(self.mp.f2, text = u"\u2212",     font=self.ft).grid(row=1,column=1,sticky="news")
        ttk.Label(self.mp.f2, text = " =  ",        font=self.ft).grid(row=1,column=3,sticky="news")
        ttk.Label(self.mp.f2, text = u" 绝对值为 ", font=self.ft).grid(row=1,column=5,sticky="news")
        for i in range(2):
            ttk.Entry(self.mp.f2,textvariable=self.anws[i],width=6,
                      font=self.ft).grid(row=1,column=2*i+4,padx=5,sticky="news")

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "减法运算后求绝对值？\n",'n')

        self.refresh()

    def refresh(self):

        self.n1 =  np.random.randint(10,100)
        self.n2 =  np.random.randint(-100,100)        
        self.ans = str(self.n1 - self.n2)
        self.abs = str(abs(self.n1 - self.n2))              

        self.var1.set(self.n1)
        if self.n2 > 0 :
           self.var2.set(self.n2)
        else:
           self.var2.set( "(" + str(self.n2) +")" )
        [self.anws[i].set("") for i in range(2)]

    def grade(self):

        ans =  [ self.anws[i].get().replace(" ","")  for i in range(2)]
        if ans[0] == self.ans and ans[1] == self.abs :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s，%s " % (self.ans, self.abs)  

        return [flag, message]

#-------------------------------------------
class  AbsMix(AbstractProblem):

    def __init__(self,root,ft):

        # multiple choices with 3 choices
        self.nchc    = 3
        self.chc     = IntVar()   # choice by user
        self.dict    = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E'}
        self.lists   = ['  <  ', '  =  ',  '  >  ' ]
        self.var1    = StringVar()
        self.var2    = StringVar()
        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2, textvariable = self.var1, font=self.ft).grid(row=0,column=0,sticky="news")
        ttk.Label(self.mp.f2, textvariable = self.var2, font=self.ft).grid(row=0,column=2,sticky="news")
        ttk.Label(self.mp.f2, text         = "   ? ",     font=self.ft).grid(row=0,column=1,sticky="news")

        for i in range(self.nchc):
            ttk.Radiobutton(self.mp.f2, text = self.lists[i],
                    variable=self.chc, value=i).grid(row=i+1,column=1,sticky="news")

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", " 绝对值大小比较。\n ",'n')

        self.refresh()

    def refresh(self):
        
        self.nums  = np.random.randint(100,200,4)
        a =  abs(self.nums[0]-self.nums[1])
        b =  abs(self.nums[2]-self.nums[3])
        if a < b :
            self.ind = 0
        elif a > b :
            self.ind = 2
        else: 
            self.ind = 1 

        self.var1.set( "|%d \u2212 %d|" %(self.nums[0], self.nums[1]) )
        self.var2.set( "|%d \u2212 %d|" %(self.nums[2], self.nums[3]) )
        self.chc.set(5)

    def grade(self):

        if self.chc.get() == self.ind :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % self.dict[self.ind]

        return [flag, message]

#-----------------------------------
