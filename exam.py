#!/usr/bin/env python
# -*- coding: utf-8 -*-

# practice exam problems

from tkinter import *
from tkinter import ttk
from tkinter import font
import numpy as np
import fractions

root  = Tk()
root.title(u"小学升初中数学模拟考试")
root.columnconfigure(0,weight=1)

ft2 = font.Font(family='Helvetica', size=16, weight='bold')

intro = StringVar()
intro.set("准备好了吗？请点击 \"开始\" 键开始模拟考试。\n\
（注意：您在点击\"下一题\"将后无法返回之前的题目）\n\
 天道酬勤，祝小朋友旗开得胜！")

# padding=(left,top,right,bottom)
mathframe = ttk.Frame(root,padding=(20,20))
mathframe.grid(row=1,column=0,sticky=(N, W, E, S))

prob_text = Text(mathframe, height=5)   #, witdh=60)
prob_text.tag_config("n", font=ft2, foreground="red",justify=CENTER)
prob_text.insert("1.0", intro.get(), "n")  #"%d.%d" % (line, column) 
prob_text.grid()

nprobs = 2    # total number of problems
str_var_arr =  [StringVar() for i in range(nprobs)]
str_var_arr[0].set(u"一对球鞋原价 %d 元，现在降价 1/5，现在售价是多少（精确到一元即可）?" % (np.random.randint(50,200)))
str_var_arr[1].set(u"2003年中国农村人均纯收入为2622元，2002年为2476元。\n2003年比2002年增长了多少百分点？")

scores       = 0.0       
next_prob    = 0     
nprob_corret = 0 
nprob_wrong  = 0 
nprob_finish = 0

def exam_loop():
    scores        = 0.0
    next_prob     = 0
    nprob_corret  = 0
    nprob_wrong   = 0
    print(next_prob)
    return

def next():
    global scores
    global next_prob
    global nprob_correct
    global nprob_wrong
    global nprob_finish
    global nprobs
    prob_text.delete("0.0","end")
    if next_prob == nprobs :
        prob_text.insert("0.0", u"您已经完成所有题目\n请按\"退出\"键结束模拟考试", "n")
    else:
        prob_text.insert("0.0", str_var_arr[next_prob].get(), "n")
        next_prob += 1
    return

# a frame on the last row containing buttons
btnframe = ttk.Frame(root,padding=(20,20))
btnframe.rowconfigure(0,weight=1)
btnframe.grid(row=3, column=0,sticky=(N, W, E, S))

ttk.Button(btnframe,text=u"开始 (start)",command=exam_loop ).grid(row=0,column=0)
ttk.Button(btnframe,text=u"下一题 (continue)",command=next ).grid(row=0,column=1)
ttk.Button(btnframe,text=u"刷新 (refresh)",command=root.quit  ).grid(row=0,column=2)
ttk.Button(btnframe,text=u"退出 (quit)   ",command=root.quit).grid(row=0,column=3)


root.mainloop()

