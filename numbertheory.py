# the module files for nubmertheory section

from basegui import * 
from random  import shuffle
import random
import fractions
#----------------------------------
# 314500 -- 3.145x10^5
def convert(num):

    sups={ u'1': u'\xb9',
           u'0': u'\u2070',
           u'3': u'\xb3',
           u'2': u'\xb2',
           u'5': u'\u2075',
           u'4': u'\u2074',
           u'7': u'\u2077',
           u'6': u'\u2076',
           u'9': u'\u2079',
           u'8': u'\u2078' }

    b   = "%E" % num
    l   = b.split('E')[0].rstrip('0')
    r   = b.split('E')[1].replace('+','').lstrip('0')
    sci = ''.join([ sups[i]  for i in r])
    ans = l+u'\u00D7'+"10"+sci

    return [ans, l, r] 

#-----------------------------------
# inverse of convert, i.e., ('3.145', '5') -->  '314500'
def  convert_inv(l,r):   # both l, and r are strings

    pow   = int(r)
    [i,d] =  l.split('.')
    if len(d) <= pow:
        ans = i + d + "0"*(pow - len(d))
    else:
        ans = i + d[:pow] + '.' + d[pow:]

    return ans

#------------------------------------
# test if a number is prime or not
def prime_Q(n):
    for m in range(2, int(n**0.5)+1):
        if not n%m:
            return False
    return True

#-----------------------------------
# decompose a number into a list of prime factors
def prime_decom(n):
    result = []
    for i in range(2,n+1):
        if prime_Q(i):
            while n % i == 0 :
                result.append(str(i))
                n /= i
        if n == 1 : break
    return result   
    
#-------------
class  ScientificNotation(AbstractProblem):

    def __init__(self,root,ft):

        # multiple choices with 5 choices
        self.choices = [ StringVar() for i in range(5) ]
        self.chc     = IntVar()   # choice by user
        self.dict = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E'}

        AbstractProblem.__init__(self,root,ft)

        for i in range(5):
            ttk.Radiobutton(self.mp.f2, textvariable = self.choices[i],
                    variable=self.chc, value=i).grid(row=i,column=0,sticky="news")
        self.refresh()

    def refresh(self):

        self.num  = np.random.randint(100,10000)*10**(np.random.randint(2,5))
        [sci,l,r] = convert(self.num)   # ['3.14*10^5', 3.14, 5]

        self.lists = [ convert_inv(l,str(int(r)+i)) for i in range(-2,3)]
        shuffle(self.lists)
        self.ind   =  self.lists.index(convert_inv(l,r))
        for i in range(5):
            self.choices[i].set(  self.dict[i] + ".  " + self.lists[i] )

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "按照科学计数法，数字 %s 与下面那个数字等价？" % (sci),'n')
        self.chc.set(5)

    def grade(self):

        if self.chc.get() == self.ind :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % self.dict[self.ind]

        return [flag, message]

#-------------------------------------
# five numbers in scientific notation, choose the largest one?

class  CompareSN(AbstractProblem):

    def __init__(self,root,ft):

        # multiple choices with 5 choices
        self.choices = [ StringVar() for i in range(5) ]
        self.chc     = IntVar()   # choice by user
        self.dict = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E'}

        AbstractProblem.__init__(self,root,ft)

        for i in range(5):
            ttk.Radiobutton(self.mp.f2, textvariable = self.choices[i],
                    variable=self.chc, value=i).grid(row=i,column=0,sticky="news")
        self.refresh()

    def refresh(self):

        self.nums = [np.random.randint(10,10000)*10**(np.random.randint(2,5)) for i in range(5)]
        self.ind  = np.argmax(self.nums) 
        self.details = [convert(self.nums[i]) for i in range(5)]
        for i in range(5):
            self.choices[i].set(  self.dict[i] + ".  " + self.details[i][0] )

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "按照科学计数法，下面那个数字最大？",'n')
        self.chc.set(5)

    def grade(self):

        if self.chc.get() == self.ind :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % self.dict[self.ind]

        return [flag, message]

#------------------------------------
class  NumberPrime(AbstractProblem):

    def __init__(self,root,ft):

        # multiple choices with 5 choices
        self.choices = [ StringVar() for i in range(5) ]
        self.chc     = IntVar()   # choice by user
        self.dict = {0:'A', 1:'B', 2:'C', 3:'D', 4:'E'}

        AbstractProblem.__init__(self,root,ft)

        for i in range(5):
            ttk.Radiobutton(self.mp.f2, textvariable = self.choices[i],
                    variable=self.chc, value=i).grid(row=i,column=0,sticky="news")
        self.refresh()

    def refresh(self):

        self.nums = np.random.randint(10,120,4) 

        self.ind = 0
        for i in range(4):
            if prime_Q(self.nums[i]):
               self.ind += 1

        for i in range(5):
            self.choices[i].set(  self.dict[i] + ".  " + str(i) + " 个" )

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "下面四个整数里有多少个素数？\n",'n')
        self.mp.t1.insert("end", "%d,  %d,  %d,  %d " % (self.nums[0], self.nums[1],
                               self.nums[2],self.nums[3])  ,'n')
        self.chc.set(5)

    def grade(self):

        if self.chc.get() == self.ind :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % self.dict[self.ind]

        return [flag, message]

# how many different prime factors does the following number contain?  
class  PrimeFactors(AbstractProblem):

    def __init__(self,root,ft):

        # multiple choices with 5 choices
        self.choices = [ StringVar() for i in range(4) ]
        self.chc     = IntVar()   # choice by user
        self.dict    = { 0:'A', 1:'B', 2:'C', 3:'D', 4:'E' }
        self.plist   = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31 ]

        AbstractProblem.__init__(self,root,ft)

        for i in range(4):
            ttk.Radiobutton(self.mp.f2, textvariable = self.choices[i],
                    variable=self.chc, value = i+1 ).grid(row=i,column=0,sticky="news")

        self.refresh()

    def refresh(self):

        while True  :
          self.ind = np.random.randint(1,5)   # number of unique prime factors
          plist    = random.sample(self.plist,self.ind)
          powlist  = np.random.randint(1,3,self.ind)

          self.num = 1
          for i in range(self.ind):
             self.num *= plist[i]**powlist[i]
          if 20 <= self.num and self.num <= 300 :
              break

        for i in range(4):
            self.choices[i].set(  self.dict[i] + ".  " + str(i+1) + " 个" )

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "整数 %d 有多少个不同的素数因子？" % self.num  ,'n')
        self.chc.set(5)

    def grade(self):

        if self.chc.get() == self.ind :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s " % self.dict[self.ind-1]

        return [flag, message]
      
#----------------------------------
class PrimeFactorization(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=0,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=20).grid(column=1,row=0)

        self.refresh()

    def refresh(self):

        self.num  = np.random.randint(30,128)  
        self.list = prime_decom(self.num)
 
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "请将整数 %d 拆解成素数因子的乘积。\n 请将素数因子由小至大排序，并用乘号＊分隔不同的因子（比如 12=2*2*3）。" % self.num  ,'n')
        self.ans.set("")

    def grade(self):
        
        tmp = "*".join(self.list) 
        ans = self.ans.get().replace(" ","")
        
        if tmp == ans :
           flag = True
           message = "正确"
        else:
           flag = False
           message = "错误。应为 %s" % tmp

        return [flag, message]

#---------------------------------
# gcd(20,40) =        
class  GreatestCommonDivisor(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=0,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=20).grid(column=1,row=0)

        self.refresh()

    def refresh(self):

        self.nums  = np.random.randint(15,128,2)*np.random.randint(2,6) 
 
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "求整数 %d 和%d 的最大公因子。" % (self.nums[0],self.nums[1]) ,'n')
        self.ans.set("")

    def grade(self):
                
        ans = self.ans.get().replace(" ","")
        t0  = fractions.gcd(self.nums[0],self.nums[1]) 
        flag    = False
        message = "错误。应为 %d " % t0 
        if  ans.isdigit():
           if int(ans) == t0: 
               flag = True
               message = "正确"
        return [flag, message]

#---------------------------------
# gcd(20,40) =        
class  GreatestCommonDivisorApp(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=0,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=20).grid(column=1,row=0)

        self.refresh()

    def refresh(self):

        self.nums  = np.random.randint(2,8,2)*np.random.randint(2,6)*np.random.randint(2,6)  
 
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "小峰同学要过生日了。妈妈准备了 %d 块披萨 和 %d 瓶饮料。" % (self.nums[0],self.nums[1]) ,'n')
        self.mp.t1.insert("end", "为了让每个小朋友都分到一样多的东西，又把所有的东西都吃掉。" ,'n')
        self.mp.t1.insert("end", "请问最多可以有多少小朋友（包括小明）参加生日会?", 'n')
        self.ans.set("")

    def grade(self):
                
        ans = self.ans.get().replace(" ","")
        t0  = fractions.gcd(self.nums[0],self.nums[1]) 
        flag    = False
        message = "错误。应为 %d " % t0 
        if  ans.isdigit():
           if int(ans) == t0: 
               flag = True
               message = "正确"
        return [flag, message]

#---------------------------------
# lcm(20,40) =        
class  LeastCommonMultiple(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=0,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=20).grid(column=1,row=0)

        self.refresh()

    def refresh(self):

        self.nums  = np.random.randint(10,72,2)
 
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "求整数 %d 和%d 的最小公倍数。" % (self.nums[0],self.nums[1]) ,'n')
        self.ans.set("")

    def grade(self):
                
        ans = self.ans.get().replace(" ","")
        s0  = fractions.gcd(self.nums[0],self.nums[1]) 
        t0  = self.nums[0]*self.nums[1]/s0 
        flag    = False
        message = "错误。应为 %d " % t0 
        if  ans.isdigit():
           if int(ans) == t0: 
               flag = True
               message = "正确"

        return [flag, message]


#         
class  LeastCommonMultipleApp(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=0,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=20).grid(column=1,row=0)

        self.refresh()

    def refresh(self):

        while True:
            self.nums  = np.random.randint(2,10,2)*np.random.randint(2,5)*5
            if self.nums[0] != self.nums[1]:
                break
 
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "小峰同学学校的五年级每班有 %d 个学生，而六年级每班有 %d 个学生。" % (self.nums[0],self.nums[1]) ,'n')
        self.mp.t1.insert("end", "已知五年级和六年级的学生总数是一样的。" ,'n')
        self.mp.t1.insert("end", "请问五或六年级全年级\"至少\"有多少学生?（只需一个数字） ", 'n')
        self.ans.set("")

    def grade(self):
                
        ans = self.ans.get().replace(" ","")
        s0  = fractions.gcd(self.nums[0],self.nums[1]) 
        t0  = self.nums[0]*self.nums[1]/s0 
        flag    = False
        message = "错误。应为 %d " % t0 
        if  ans.isdigit():
           if int(ans) == t0: 
               flag = True
               message = "正确"

        return [flag, message]
#-------------------------------------
