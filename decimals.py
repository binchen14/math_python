from tkinter import *
from tkinter import ttk
from basegui import *
import random
import numpy as np
import fractions

class Tenths(Canvas):

    def __init__(self,root):

        self.x0     = 20
        self.y0     = 20
        self.dx     = 15
        self.height = 150

        Canvas.__init__(self,root)
        self.grid(column=0, row=0, sticky=(N, W, E, S))

        self.refresh()

    def refresh(self):

        self.ncols = np.random.randint(1,10)
        self.grid = [ self.create_rectangle(( self.x0 +     i*self.dx, self.y0, 
                                              self.x0 + (i+1)*self.dx, self.y0 + self.height), 
                                              fill="red")       for i in range(self.ncols)]
        self.grid2 = [ self.create_rectangle(( self.x0 +     i*self.dx, self.y0, 
                                              self.x0 + (i+1)*self.dx, self.y0 + self.height), 
                                              fill="white")       for i in range(self.ncols,10)]
#--------------------------------
class Hundredths(Canvas):

    def __init__(self,root):
        self.x0     = 20
        self.y0     = 20
        self.dx     = 15
        self.dy     = 15

        Canvas.__init__(self,root)
        self.grid(column=0, row=0, sticky=(N, W, E, S))

        self.refresh()

    def refresh(self):

        self.ncols = np.random.randint(1,10)
        self.nrows = np.random.randint(1,10)

        self.grid1 = [[ self.create_rectangle((self.x0 +     i*self.dx , self.y0 +     j*self.dy,
                                              self.x0 + (i+1)*self.dx,  self.y0 + (j+1)*self.dy ), fill="red")
                      for i in range(self.ncols) ]  for j in range(10)] 

        self.grid2 = [[ self.create_rectangle((self.x0 +     i*self.dx , self.y0 +     j*self.dy,
                                              self.x0 + (i+1)*self.dx,  self.y0 + (j+1)*self.dy ), fill ="white")
                      for i in range(self.ncols+1,10) ]  for j in range(10)] 

        self.grid3 = [[ self.create_rectangle((self.x0 +     i*self.dx , self.y0 +     j*self.dy,
                                              self.x0 + (i+1)*self.dx,  self.y0 + (j+1)*self.dy ), fill="red")
                      for i in range(self.ncols,self.ncols+1)]   for j in range(self.nrows)] 

        self.grid4 = [[ self.create_rectangle((self.x0 +     i*self.dx , self.y0 +     j*self.dy,
                                              self.x0 + (i+1)*self.dx,  self.y0 + (j+1)*self.dy ), fill="white")
                      for i in range(self.ncols,self.ncols+1)]   for j in range(self.nrows,10)] 

# defintions of tenths using images
class   DecimalTenths(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        self.figure = Tenths(self.mp.f2)        
        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=1,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=10).grid(column=2,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "假设下面大的正方形的面积为 1。\n",'n')
        self.mp.t1.insert("end", "那么红色的地方的面积用小数表示应为多少呢？" ,'n')

        self.refresh()

    def refresh(self):

        self.figure.refresh()
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        t0  = "0." + str(self.figure.ncols) 
        flag    = False
        message = "错误。应为 " + t0
        if  ans == t0 :
           flag = True
           message = "正确"
        return [flag, message]


# defintions of tenths using images
class   DecimalHundredths(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()

        AbstractProblem.__init__(self,root,ft)

        self.figure = Hundredths(self.mp.f2)        
        ttk.Label(self.mp.f2,text = "输入结果："  ,font=self.ft ).grid(column=1,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=10).grid(column=2,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "假设下面大的正方形的面积为 1。\n",'n')
        self.mp.t1.insert("end", "那么红色的地方的面积用小数表示应为多少呢？" ,'n')

        self.refresh()

    def refresh(self):

        self.figure.refresh()
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        t0  = "0." + str(self.figure.ncols) + str(self.figure.nrows) 
        flag    = False
        message = "错误。应为 " + t0
        if  ans == t0 :
           flag = True
           message = "正确"
        return [flag, message]


# decimals to fractions, need simplify it
class   DecimalToFrac(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()
        self.dec = StringVar()
        AbstractProblem.__init__(self,root,ft)
    
        ttk.Label(self.mp.f2,textvariable = self.dec,font=self.ft ).grid(column=1,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans,font=self.ft, width=10).grid(column=2,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "请将下面的小数写成分数，并尽可能化简。\n",'n')
        self.mp.t1.insert("end", "(比如 0.2 可以写成 1/5)" ,'n')

        self.refresh()

    def refresh(self):

        self.num   = np.random.randint(1,100)
        self.data  = "0." + str(self.num)
        self.up    = self.num
        if len(str(self.num)) == 1:
           self.down = 10
        elif len(str(self.num)) == 2:
           self.down = 100
        gcd        = fractions.gcd(self.up,self.down)
        self.up    = int(  self.up/gcd)
        self.down  = int(self.down/gcd)       
        self.frac  = str(self.up)+'/'+str(self.down)
        self.dec.set(" %s 的分数表达为：" % self.data)
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        flag    = False
        message = "错误。应为 " + self.frac
        if  ans == self.frac :
           flag = True
           message = "正确"
        return [flag, message]

# round decimals say, 3.47 --> 3.5
class   RoundDecimal(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()
        self.dec = StringVar()

        AbstractProblem.__init__(self,root,ft)
    
        ttk.Label(self.mp.f2,textvariable = self.dec,font=self.ft ).grid(column=1,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans,font=self.ft, width=10).grid(column=2,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "小数四舍五入练习。\n",'n')

        self.refresh()

    def refresh(self):

        dict = ['十分位','百分位','千分位']

        self.ints   = np.random.randint(1,1000)
        self.digits = np.random.randint(1,10,6)
        #self.digits = [1,9,9,9,9,9]
        self.chars  = ''.join([ str(self.digits[i]) for i in range(6) ] )
        
        self.data   = str(self.ints) +  "." + self.chars
        self.ind    = int( np.random.uniform(0,1)*100 ) // 33
        self.good   = str(round( float(self.data), self.ind + 1))           
        # numpy drop trailing zeros, need be fixed
        ndrop       = len(self.data) - len(self.good) - (6 - 1 - self.ind)  
        if ndrop != 0:
            self.good  += "0"*ndrop

        self.dec.set(" %s   精确到%s后等于：" % (self.data, dict[self.ind]) )
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        flag    = False
        message = "错误。应为 " + self.good
        if  ans == self.good :
           flag = True
           message = "正确"
        return [flag, message]


# round decimals say, 3.47 --> 3.5
class  AddTwoDecimals(AbstractProblem):

    def __init__(self,root,ft):

        self.ans = StringVar()
        self.operand1 = StringVar()
        self.operand2 = StringVar()

        AbstractProblem.__init__(self,root,ft)
    
        ttk.Label(self.mp.f2,textvariable = self.operand1,font=self.ft).grid(column=1,row=0,padx=10)
        ttk.Label(self.mp.f2,text = " + ",                font=self.ft).grid(column=2,row=0,padx=10)
        ttk.Label(self.mp.f2,textvariable = self.operand2,font=self.ft).grid(column=3,row=0,padx=10)
        ttk.Label(self.mp.f2,text = " = ",                font=self.ft).grid(column=4,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans,font=self.ft, width=10).grid(column=5,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "小数加法练习。\n",'n')

        self.refresh()

    def refresh(self):

        dict = ['十分位','百分位','千分位']

        self.int1   = np.random.randint(1,10)
        self.int2   = np.random.randint(1,10)
        self.dig1   = np.random.randint(1,10,3)
        self.dig2   = np.random.randint(1,10,3)
        self.char1  = ''.join([ str(self.dig1[i]) for i in range(3) ] )
        self.char2  = ''.join([ str(self.dig2[i]) for i in range(3) ] )
        
        self.data1  = str(self.int1) +  "." + self.char1
        self.data2  = str(self.int2) +  "." + self.char2
        self.good   = str( round( float(self.data1) + float(self.data2), 3) ) 
        ndrop       = 3 - len(self.good.split('.')[1])  
        if ndrop != 0:
            self.good  += "0"*ndrop

        self.operand1.set(self.data1)
        self.operand2.set(self.data2)
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        flag    = False
        message = "错误。应为 " + self.good
        if  ans == self.good :
           flag = True
           message = "正确"
        return [flag, message]

#  a - b  
class  SubTwoDecimals(AbstractProblem):

    def __init__(self,root,ft):

        self.ans      = StringVar()
        self.operand1 = StringVar()
        self.operand2 = StringVar()

        AbstractProblem.__init__(self,root,ft)
    
        ttk.Label(self.mp.f2,textvariable = self.operand1,font=self.ft).grid(column=1,row=0,padx=10)
        ttk.Label(self.mp.f2,text = u" \u2212 ",          font=self.ft).grid(column=2,row=0,padx=10)
        ttk.Label(self.mp.f2,textvariable = self.operand2,font=self.ft).grid(column=3,row=0,padx=10)
        ttk.Label(self.mp.f2,text = " = ",                font=self.ft).grid(column=4,row=0,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans,font=self.ft, width=10).grid(column=5,row=0)

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "小数减法练习。\n",'n')

        self.refresh()

    def refresh(self):

        self.int2   = np.random.randint(1,10)
        self.int1   = self.int2 + np.random.randint(1,10)
        self.dig1   = np.random.randint(1,10,3)
        self.dig2   = np.random.randint(1,10,3)
        self.char1  = ''.join([ str(self.dig1[i]) for i in range(3) ] )
        self.char2  = ''.join([ str(self.dig2[i]) for i in range(3) ] )
        
        self.data1  = str(self.int1) +  "." + self.char1
        self.data2  = str(self.int2) +  "." + self.char2
        self.good   = str( round( float(self.data1) - float(self.data2), 3) ) 
        ndrop       = 3 - len(self.good.split('.')[1])  
        if ndrop != 0:
            self.good  += "0"*ndrop

        self.operand1.set(self.data1)
        self.operand2.set(self.data2)
        self.ans.set("")

    def grade(self):

        ans = self.ans.get().replace(" ","")
        flag    = False
        message = "错误。应为 " + self.good
        if  ans == self.good :
           flag = True
           message = "正确"
        return [flag, message]
   
# error analysis commuting roundup and addition operation
class  RoundCommutator(AbstractProblem):

    def __init__(self,root,ft):

        self.ans1     = StringVar()
        self.ans2     = StringVar()
        self.ans3     = StringVar()
        self.ans4     = StringVar()
        self.operand1 = StringVar()
        self.operand2 = StringVar()
        self.chc      = IntVar()   # choice by user
        AbstractProblem.__init__(self,root,ft)
    
        ttk.Label(self.mp.f2,textvariable = self.operand1,font=self.ft).grid(column=1,row=0,padx=10)
        ttk.Label(self.mp.f2,text = u" + ",               font=self.ft).grid(column=1,row=1,padx=10)
        ttk.Label(self.mp.f2,textvariable = self.operand2,font=self.ft).grid(column=1,row=2,padx=10)
        ttk.Label(self.mp.f2,text = " || ",               font=self.ft).grid(column=1,row=3,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans1,font=self.ft, width=10).grid(column=1,row=4)
        ttk.Label(self.mp.f2,text = " 四舍五入到十分位等于--> ",font=self.ft).grid(column=2,row=0,padx=10)
        ttk.Label(self.mp.f2,text =  "四舍五入到十分位等于--> ",font=self.ft).grid(column=2,row=2,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans2,font=self.ft, width=10).grid(column=3,row=0)
        ttk.Entry(self.mp.f2,textvariable = self.ans3,font=self.ft, width=10).grid(column=3,row=2)
        ttk.Label(self.mp.f2,text = u" + ",               font=self.ft).grid(column=3,row=1,padx=10)
        ttk.Label(self.mp.f2,text = " || ",                font=self.ft).grid(column=3,row=3,padx=10)
        ttk.Entry(self.mp.f2,textvariable = self.ans4,font=self.ft, width=10).grid(column=3,row=4)
        ttk.Label(self.mp.f2,text = " 四舍五入到十分位是否等于--> ",font=self.ft).grid(column=2,row=4,padx=10)
        ttk.Label(self.mp.f2,text = u" ? (请选择是或否) ", font=self.ft).grid(column=4,row=4,padx=10)

        ttk.Radiobutton(self.mp.f2, text = "  是  ", variable=self.chc, value=0).grid(row=5,column=4,padx=15,sticky="news")
        ttk.Radiobutton(self.mp.f2, text = "  否  ", variable=self.chc, value=1).grid(row=6,column=4,padx=15,sticky="news")
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "思考题：四则运算前／后四舍五入的误差分析。\n",'n')
        self.mp.t1.insert("end", "比较先四舍五入后相加的结果与先加法运算后四舍五入的结果\n",'n')
        self.mp.t1.insert("end", "想想什么时候结果相同，什么时候不同？\n",'n')

        self.refresh()

    def refresh(self):

        self.int1   = np.random.randint(1,10)
        self.int2   = np.random.randint(1,10)
        self.dig1   = np.random.randint(1,10)
        self.dig2   = np.random.randint(1,10)
        self.char1  = str(self.dig1)+"5"
        self.char2  = str(self.dig2)+"5"
        
        self.data1  = str(self.int1) +  "." + self.char1
        self.data2  = str(self.int2) +  "." + self.char2
        self.good   = str( round( float(self.data1) + float(self.data2), 2) ) 
        ndrop       = 2 - len(self.good.split('.')[1])  
        if ndrop != 0:
            self.good  += "0"*ndrop

        self.appx1  = round(float(self.data1)+0.001, 1) # round(4.55,1) -->4.5 
        self.appx2  = round(float(self.data2)+0.001, 1) 
        self.appx3  = round( self.appx1 + self.appx2, 1)

        self.good1  = str(self.appx1)
        self.good2  = str(self.appx2)
        self.good3  = str(self.appx3)

        self.operand1.set(self.data1)
        self.operand2.set(self.data2)
        self.ans1.set("")
        self.ans2.set("")
        self.ans3.set("")
        self.ans4.set("")
        self.chc.set(3)

    def grade(self):

        ans1 = self.ans1.get().replace(" ","")
        ans2 = self.ans2.get().replace(" ","")
        ans3 = self.ans3.get().replace(" ","")
        ans4 = self.ans4.get().replace(" ","")

        flag1 = (ans1 == self.good)
        flag2 = (ans2 == self.good1)
        flag3 = (ans3 == self.good2)
        flag4 = (ans4 == self.good3)
        flag5 = (self.chc.get() == 1)

        if flag1 and flag2 and flag3 and flag4:
            flag    = True
            message = "正确"
        else:
            flag = False
            message = "错误"
        return [flag, message]
 
#-------------------------------------
if __name__ == "__main__" :
    
    root = Tk()
    root.rowconfigure(0,weight=1)
    ft1 = font.Font(family='Helvetica', size=20, weight='bold')
    #Tenths(root)
    #Hundredths(root)
    #DecimalTenths(root,ft1)
    DecimalHundredths(root,ft1)
    root.mainloop()
