#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import ttk
from tkinter import  *
from tkinter import font
import numpy as np
import fractions
from basegui      import *
from numbertheory import *
from decimals     import *
from wholenumber  import *
from geometry     import *
from frac         import *

#----------------------------------------------
# create the menus for the math exercises
class Menubar(Menu):

    def __init__(self,root):  

        self.root = root
        self.fnt1 = font.Font(family='Helvetica', size=20, weight='bold')
        Menu.__init__(self, root)  # self is the menu-bar
        math = Menu(self, tearoff=0)
        mathitems = [[ Menu(math,tearoff=0) for j in range(2)] for i in range(6)]

        grade_6_I = ['数论','整数','有理数','小数','分数','混合运算','几何','统计']

        for i in range(1,7):
          for j in range(1,3):
              math.add_cascade(label = "%d 年级 %d 学期" % (i,j), 
                               menu  = mathitems[i-1][j-1]  ) 
          math.add_separator()    
   
        menu60 = [ Menu(mathitems[5][0],tearoff=0) for i in range(len(grade_6_I)) ]   
        for i in range(len(grade_6_I)): 
            mathitems[5][0].add_cascade(label = grade_6_I[i],menu= menu60[i])

        menu60[0].add_command(label="科学计数法",   command=self.invoke_scientific_notation)
        menu60[0].add_command(label="科学计数法(最大值)",  command=self.invoke_scientific_compare)
        menu60[0].add_command(label="素数",     command=self.invoke_num_prime)
        menu60[0].add_command(label="素数因子",     command=self.invoke_prime_factors)
        menu60[0].add_command(label="素数因子拆解", command=self.invoke_prime_factorization)
        menu60[0].add_command(label="最大公因子",command=self.invoke_gcd)
        menu60[0].add_command(label="最大公因子应用题",command=self.invoke_gcd_app)
        menu60[0].add_command(label="最小公倍数",command=self.invoke_lcm)
        menu60[0].add_command(label="最小公倍数应用题",command=self.invoke_lcm_app)
        menu60[1].add_command(label="正负数排序",    command=self.invoke_sort_integers)
        menu60[1].add_command(label="绝对值",    command=self.invoke_abs_sub)
        menu60[1].add_command(label="绝对值混合运算",  command=self.invoke_abs_mix)
        menu60[3].add_command(label="小数十分位定义",command=self.invoke_tenth)
        menu60[3].add_command(label="小数百分位定义",command=self.invoke_hundredth)
        menu60[3].add_command(label="小数转换分数",command=self.invoke_dec_to_frac)
        menu60[3].add_command(label="小数四舍五入",command=self.invoke_round_dec)
        menu60[3].add_command(label="小数加法",   command=self.invoke_add2dec)
        menu60[3].add_command(label="小数减法",   command=self.invoke_sub2dec)
        menu60[3].add_command(label="误差分析",   command=self.invoke_round_commutator)
        menu60[4].add_command(label="等价分数",  command=self.invoke_eqfrac)
        menu60[4].add_command(label="分数加减",  command=self.invoke_addsubfrac)
        menu60[4].add_command(label="最小公分母",command=self.invoke_lcd)
        menu60[4].add_command(label="约分",      command=self.invoke_sim_frac)
        menu60[4].add_command(label="分数乘法",  command=self.invoke_frac_prod)
        menu60[4].add_command(label="分数除法",  command=self.invoke_frac_divide)
        menu60[4].add_command(label="矩形边长",  command=self.invoke_rect_sidelength)
        menu60[4].add_command(label="三角形边长", command=self.invoke_trig_sidelength)
        menu60[6].add_command(label="四边形的区分", command=self.invoke_quadrilaterals)
        menu60[6].add_command(label="四边形(思考题)", command=self.invoke_quadri_intersect)
        menu60[6].add_command(label="扇环面积", command=self.invoke_annulus_area)
        menu60[6].add_command(label="圆的面积应用题", command=self.invoke_circle_area)
        menu60[6].add_command(label="扇形",     command=self.invoke_sector_area_circ)
        menu60[6].add_command(label="圆周率",     command=self.invoke_half_circle)
        menu60[6].add_command(label="圆柱体",     command=self.invoke_cyl_area_vol)
        menu60[6].add_command(label="圆柱体面积",     command=self.invoke_cyl_area)
        menu60[6].add_command(label="圆锥体积",       command=self.invoke_cone_vol)
        menu60[6].add_command(label="球的体积",       command=self.invoke_ball_vol)
        menu60[6].add_command(label="长方体面积体积", command=self.invoke_cuboid)
        menu60[6].add_command(label="长方体体积", command=self.invoke_cuboid_vol)
        menu60[6].add_command(label="长方体面积", command=self.invoke_cuboid_area)
        menu60[6].add_command(label="正方体", command=self.invoke_cube)

        self.add_cascade(label="数学", menu=math)
        root.config(menu=self)

    def donothing(self):    
        pass
   
    def invoke_eqfrac(self):
        EqualFractions(self.root, self.fnt1)

    def invoke_addsubfrac(self):
        AddSubFracs(self.root, self.fnt1)
 
    def invoke_lcd(self):
        LeastCommonDenominator(self.root,self.fnt1)

    def invoke_lcm(self):
        LeastCommonMultiple(self.root,self.fnt1)

    def invoke_lcm_app(self):
        LeastCommonMultipleApp(self.root,self.fnt1)

    def invoke_gcd(self):
        GreatestCommonDivisor(self.root,self.fnt1)

    def invoke_gcd_app(self):
        GreatestCommonDivisorApp(self.root,self.fnt1)

    def invoke_sim_frac(self):
        SimplifyFraction(self.root,self.fnt1)

    def invoke_frac_prod(self):
        MultiplyTwoFractions(self.root,self.fnt1)

    def invoke_frac_divide(self):
        DivideTwoFractions(self.root,self.fnt1)

    def invoke_rect_sidelength(self):
        RectangleSideLengths(self.root,self.fnt1)

    def invoke_trig_sidelength(self):
        TriangleSideLengths(self.root,self.fnt1)

    def invoke_scientific_notation(self):
        ScientificNotation(self.root,self.fnt1)

    def invoke_scientific_compare(self):
        CompareSN(self.root,self.fnt1)

    def invoke_num_prime(self):
        NumberPrime(self.root,self.fnt1)

    def invoke_prime_factors(self):
        PrimeFactors(self.root,self.fnt1)

    def invoke_prime_factorization(self):
        PrimeFactorization(self.root,self.fnt1)

    def invoke_tenth(self):
        DecimalTenths(self.root,self.fnt1)

    def invoke_hundredth(self):
        DecimalHundredths(self.root,self.fnt1)

    def invoke_dec_to_frac(self):
        DecimalToFrac(self.root,self.fnt1)

    def invoke_round_dec(self):
        RoundDecimal(self.root,self.fnt1)

    def invoke_add2dec(self):
        AddTwoDecimals(self.root,self.fnt1)

    def invoke_sub2dec(self):
        SubTwoDecimals(self.root,self.fnt1)

    def invoke_round_commutator(self):
        RoundCommutator(self.root,self.fnt1)

    def invoke_sort_integers(self):
        SortIntegers(self.root,self.fnt1)

    def invoke_abs_sub(self):
        AbsSub(self.root,self.fnt1)

    def invoke_abs_mix(self):
        AbsMix(self.root,self.fnt1)

    def invoke_cuboid(self):
        CuboidAreaVol(self.root,self.fnt1)

    def invoke_cuboid_area(self):
        CuboidSideArea(self.root,self.fnt1)

    def invoke_circle_area(self):
        CircleArea(self.root,self.fnt1)

    def invoke_half_circle(self):
        HalfCircle(self.root,self.fnt1)

    def invoke_sector_area_circ(self):
        SectorAreaCirc(self.root,self.fnt1)

    def invoke_cyl_area_vol(self):
        CylinderAreaVol(self.root,self.fnt1)

    def invoke_cyl_area(self):
        CylinderArea(self.root,self.fnt1)

    def invoke_cone_vol(self):
        ConeVol(self.root,self.fnt1)

    def invoke_ball_vol(self):
        BallVol(self.root,self.fnt1)

    def invoke_cuboid_vol(self):
        CuboidVol(self.root,self.fnt1)

    def invoke_cube(self):
        Cub(self.root,self.fnt1)

    def invoke_quadrilaterals(self):
        Quadrilaterals(self.root,self.fnt1)

    def invoke_quadri_intersect(self):
        QuadrilateralsIntersect(self.root,self.fnt1)

    def invoke_annulus_area(self):
        Annulus_Area(self.root,self.fnt1)

if __name__ == '__main__':
    root = Tk()
    root.title("小学数学练习")
    root.rowconfigure(0,weight=1)
    root.columnconfigure(0,weight=1)
    ft1 = font.Font(family='Helvetica', size=20, weight='bold')
    a = Menubar(root)
    #a = MultiplyTwoFractions(root,ft1)
    #b = SimplifyFraction(root,ft1)
    #c  = LeastCommonDenominator(root,ft1)
    root.mainloop()

      
