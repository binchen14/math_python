#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import ttk
from tkinter import  *
from tkinter import font
import numpy as np

class Clock(object):

    def __init__(self,hours=0,minutes=0,seconds=0):
        self.hours   = hours
        self.minutes = minutes
        self.seconds = seconds

    def set(self, hours, minutes, seconds):
        self.hours   = hours
        self.minutes = minutes
        self.seconds = seconds

    def reset(self):
        self.hours   = 0 
        self.minutes = 0 
        self.seconds = 0 

    def tick(self):
#        time.sleep(1)
        if self.seconds != 59 :
           self.seconds +=1 
        else:
           self.seconds = 0   
           if self.minutes != 59: 
              self.minutes += 1
           else:
              self.minutes = 0 
              if self.hours == 23: 
                 self.hours = 0 
              else:
                 self.hours += 1

    def get_h(self):
        return self.hours

    def get_m(self):
        return self.minutes

    def get_s(self):
        return self.seconds

    def get_tot_secs(self):
        return self.hours*3600 + self.minutes*60 + self.seconds

    def timing(self):
        pass

    def display(self):
        print("%2d:%2d:%2d \n" % (self.hours,self.minutes,self.seconds) )

    def __str__(self):
        return " %2d : %2d : %2d " % (self.hours,self.minutes,self.seconds)

#------------------------------
# timing widget
class Timewgt(ttk.Frame):

    def __init__(self,root,ft,minutes=60):

        self.root = root 
        self.ft  = ft
        self.max = minutes
        self.c   = Clock()
        self.time = StringVar()
        self.time.set("            ")
        self.timeid = 0
        self.timeflag = False

        ttk.Frame.__init__(self,root,padding=(20,20))
        self.grid(row=0,column=1,sticky="news")         
        self.b1 = ttk.Button(self, text = "开始计时", command=self.timing)
        self.b1.grid(row=0,column=0,columnspan=3,pady=5)
        ttk.Label(self, text = "小时", font=self.ft ).grid(row=1,column=0,pady=5)
        ttk.Label(self, text = "分钟", font=self.ft ).grid(row=1,column=1,pady=5)
        ttk.Label(self, text = "秒钟", font=self.ft ).grid(row=1,column=2,pady=5)
        ttk.Label(self,textvariable=self.time,font=self.ft,relief=SUNKEN).grid(row=2,column=0,columnspan=3,pady=5)
        self.b2 = ttk.Button(self, text = "清零", command=self.cleartime,state="disabled")
        self.b2.grid(row=3,column=0,columnspan=3,pady=5)

    def timing(self):        
        self.timeflag = True    	# yes, I am timing
        self.b1.config(state='disabled')
        self.b2.config(state='normal')
        self.root.b1.config(state='normal')
        self.timing2()
        #self.time.set( str(self.c) )
        #self.timeid = self.after(1000,self.timing)
        #self.c.tick()
        #return             
  
    def timing2(self):
        self.time.set(str(self.c))
        self.timeid = self.after(1000,self.timing2)
        self.c.tick()        

    def cleartime(self):
        self.timeflag = False
        self.c.reset()
        self.time.set("            ")
        self.after_cancel(self.timeid)
        self.b1.config(state='normal')

    def reporttime(self):
        return  self.c.get_tot_secs()            

    def freezetime(self):
        try:
            self.after_cancel(self.timeid)
        except:
            pass

# control panel --- template
class ControlPanel(ttk.Panedwindow):

    """ the calling frame should provide two methods:
        [ flag, message ] = root.grade(), and root.refresh(),
          message is used to set the self.comment output   """

    def __init__(self,root,ft,num_tot=10,points_each=10):

        self.num_tot     = num_tot      # how many problems are there? 
        self.points_each = points_each  # points for each problem
        self.total_score = points_each*num_tot 
        self.num_worked  = 0 	        # no. questions already finished
        self.num_crt     = 0            # no. questions answered correctly

        self.root = root 
        self.ft   = ft
        self.c    = Clock()
        self.timeid   = 0              # used for after_cancel() method
        self.timeflag = False          # time the current exercise?
        self.correct  = StringVar()
        self.score    = StringVar()
        self.percent  = StringVar()
        self.comment  = StringVar()
        self.time     = StringVar()    # output digital time
        self.time.set(" "*10)

        ttk.Panedwindow.__init__(self,root,orient=VERTICAL)
        self.lb1 = ttk.Label(self,text = "控制中心", relief=RAISED,borderwidth=4,
                                    foreground='blue',font=self.ft)
        self.lb2 = ttk.Label(self, text = "成绩统计",relief=RAISED,borderwidth=4,
                                    foreground='blue',font=self.ft)
        self.f1  = ttk.Labelframe(self, labelwidget=self.lb1,borderwidth=4,relief=RAISED, padding=(10,10))
        self.f2  = ttk.Labelframe(self, labelwidget=self.lb2,borderwidth=4,relief=RAISED, padding=(10,10))
        self.add(self.f1)
        self.add(self.f2)

        # ---- content of the control panel ----
        self.b1 = ttk.Button(self.f1, text = "计时", command=self.timing)
        self.b2 = ttk.Button(self.f1, text = "清零", command=self.cleartime,state="disabled")
        self.b1.grid(row=0,column=0,pady=5)
        self.b2.grid(row=0,column=1,pady=5)
        ttk.Label(self.f1, text = "时 : 分 : 秒",font=self.ft, 
                  width=10).grid(row=1,column=0,columnspan=3,pady=5)
        ttk.Label(self.f1,textvariable=self.time,font=self.ft,borderwidth=5,
                  relief=SUNKEN,width=10).grid(row=2,column=0,columnspan=3,pady=5)
	
        self.b3 = ttk.Button(self.f1,text="完成",   command = self.submit,state="disabled")
        self.b4 = ttk.Button(self.f1,text="下一题", command = self.next,  state="disabled")
        self.b5 = ttk.Button(self.f1,text="退出",   command = self.killframe)
        self.b3.grid(row=3,column=0,pady=10,sticky="news")
        self.b4.grid(row=3,column=1,pady=10,sticky="news")
        self.b5.grid(row=3,column=2,pady=10,sticky="news")

        ttk.Label(self.f2,textvariable=self.comment,width=30,font=("Helvetica", 14),
                  borderwidth=5,relief=SUNKEN).grid(row=4,column=0,columnspan=3,rowspan=4,pady=5)

        ttk.Label(self.f2, text = "做对题目", font=self.ft ).grid(row=10,column=0,columnspan=2,pady=5)
        ttk.Label(self.f2, text = "已经得分", font=self.ft ).grid(row=11,column=0,columnspan=2,pady=5)
        ttk.Label(self.f2, text = "正确率为", font=self.ft ).grid(row=12,column=0,columnspan=2,pady=5)
        ttk.Label(self.f2, textvariable=self.correct,font=self.ft,relief=SUNKEN,
                  width=6,borderwidth=4).grid(row=10,column=2,pady=5)
        ttk.Label(self.f2, textvariable=self.score,  font=self.ft,relief=SUNKEN,
                  width=6,borderwidth=4).grid(row=11,column=2,pady=5)
        ttk.Label(self.f2, textvariable=self.percent,font=self.ft,relief=SUNKEN,
                  width=6,borderwidth=4).grid(row=12,column=2,pady=5)

    # b1: time; b2: clear; b3: submit; b4: next; b5: quit

    def timing(self):        
        self.timeflag = True    	  # yes, I am timing now
        self.b1.config(state='disabled')  # disable the time button now
        self.b2.config(state='normal')    # allow the clear time button
        self.b3.config(state='normal')    # allow the submit button
        self.timing2()
 
    ## should not be called from outside 
    def timing2(self):
        self.time.set(str(self.c))        # read clock object and set  time 
        self.timeid = self.after(1000,self.timing2) # schedule time read later on
        self.c.tick()                     # tick the clock for next reading 

    def cleartime(self):
        self.timeflag = False
        self.c.reset()
        self.time.set(" "*10) 
        try:
            self.after_cancel(self.timeid)    # cancel the scheduled call back
        except:
            pass
        self.b1.config(state='normal')    # re-enable the time button

    def reporttime(self):                 # total time in seconds for the exercise
        return  self.c.get_tot_secs()            

    def freezetime(self):
        try:
            self.after_cancel(self.timeid)
        except:
            pass

    def submit(self):
        self.b3.config(state='disabled')  # submit button disabled
        self.b4.config(state='normal')    # next   button enabled        
        self.num_worked += 1
        ans_flag = False
        message  = " "
        try:
            [ans_flag, message] = self.root.grade()
        except:
            print("error from the grade function!")
        if ans_flag == True:
           self.num_crt += 1
        self.correct.set("%d/%d" %(self.num_crt, self.num_worked)  )
        self.score.set("%d/%d" %(self.num_crt*self.points_each, self.num_worked*self.points_each) )
        self.percent.set("%d%%" % int( float(self.num_crt*100)/self.num_worked )  )
        self.comment.set(message)
           
    def next(self):        
        self.b4.config(state='disabled')   	# next  button disabled 
        if self.num_worked == self.num_tot:    	# are you done with all probs?
            self.b3.config(state='disabled')  	# submit button disabled
            if self.timeflag == True:
                self.freezetime()
            message = "练习全部完成！"
            self.comment.set(message)
        else:
            try:
                self.root.refresh()   # post a new problem if not finished
            except:
                pass
            self.b3.config(state="normal") 
            self.comment.set("")

    def killframe(self):            #  kill the father panel, not just itself.
        self.root.grid_forget()
        self.root.destroy()

# --------- math panel on the left -----
class MathPanel(ttk.Panedwindow):
        
    def __init__(self,root,ft):
 
        self.ft = ft   # font as instance variable

        ttk.Panedwindow.__init__(self,root,orient=VERTICAL)
        self.lb1 = ttk.Label(self,text = "数学中心", relief=RAISED,borderwidth=4,
                                    foreground='red',font=self.ft)
        self.lb2 = ttk.Label(self,text = "图片中心", relief=RAISED,borderwidth=4,
                                    foreground='red',font=self.ft)
        self.lb3 = ttk.Label(self,text = "知识中心", relief=RAISED,borderwidth=4,
                                    foreground='red',font=self.ft)
        self.f1  = ttk.Labelframe(self, labelwidget=self.lb1,borderwidth=4,relief=RAISED, padding=(10,10))
        self.f2  = ttk.Labelframe(self,labelwidget=self.lb2,borderwidth=4,relief=RAISED, padding=(10,10))
        self.f3  = ttk.Labelframe(self,labelwidget=self.lb3,borderwidth=4,relief=RAISED, padding=(10,10))
        self.add(self.f1)
        self.add(self.f2)
        self.add(self.f3)
  
        self.f2a = ttk.Frame(self.f2)
        self.f2b = ttk.Frame(self.f2)
        self.f2a.grid(row=0,column=0)
        self.f2b.grid(row=0,column=1)

        # t1 is the text box for the math problem.
        self.t1 = Text(self.f1,width=100,height=6)
        self.t1.grid(row=0,column=0,sticky="news")
        self.t1.tag_config("n", font=self.ft, foreground="red",justify=CENTER)

        #self.b2 = ttk.Button(self.f2a,text="本题不需要插图",command =root.quit,state="disabled")
        #self.b2.grid()
 
        self.b4 = ttk.Button(self.f3,text="概要",   command =root.quit,state="disabled")
        self.b4.grid()
        self.t2 = Text(self.f3,width=100,height=7)
        self.t2.grid(row=0,column=0,sticky="news")
        self.t2.tag_config("n", font=self.ft, foreground="blue",justify=CENTER)

# the class for an abstract problem: a frame consists of math + control panel     
class  AbstractProblem(ttk.Frame):
 
    def __init__(self,root,ft):

        self.ft = ft
        self.num_tot     = 5
        self.points_each = 20

        ttk.Frame.__init__(self,root) 	# frame contains the whole exercise
        self.grid(row=0,column=0,sticky="news")
        self.mp = MathPanel(self,self.ft)
        self.mp.grid(row=0,column=0,sticky="news")
        self.cp = ControlPanel(self, self.ft, self.num_tot, self.points_each)
        self.cp.grid(row=0,column=1,sticky="news")

        self.ans_a = StringVar()
        self.ans_b = StringVar()
        self.ans_c = StringVar()

    def refresh(self):
        raise NotImplementedError('refresh method must be overriden in subclass!')
               
    def grade(self):
        raise NotImplementedError('[flag,message]=grade() must be overriden in subclass! ')
        #return [flag, message]

