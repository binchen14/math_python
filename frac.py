#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import ttk
from tkinter import  *
from tkinter import font
import numpy as np
import fractions
from basegui      import *
from numbertheory import *
from decimals     import *
from wholenumber  import *
from geometry     import *
#-----------------------------------------------------
# side length of a triangle
class TriangleSideLengths(AbstractProblem):
 
    def __init__(self,root,ft):

        self.trig_circ = 0
        self.trig_a    = 0 
        self.trig_b    = 0 
        self.trig_c    = 0

        self.ans_a = StringVar()
        self.ans_b = StringVar()
        self.ans_c = StringVar()

        AbstractProblem.__init__(self,root,ft)

        ttk.Label(self.mp.f2, text="边长 a = ", width=8, borderwidth=4,
                  font=self.ft).grid(row=2,column=2,padx=5)
        ttk.Label(self.mp.f2, text="边长 b = ", width=8, borderwidth=4,
                  font=self.ft).grid(row=3,column=2,padx=5)
        ttk.Label(self.mp.f2, text="边长 c = ", width=8, borderwidth=4,
                  font=self.ft).grid(row=4,column=2,padx=5)
        ttk.Label(self.mp.f2, text="厘米",  borderwidth=4,
                  font=self.ft).grid(row=2,column=4,padx=5)
        ttk.Label(self.mp.f2, text="厘米",  borderwidth=4,
                  font=self.ft).grid(row=3,column=4,padx=5)
        ttk.Label(self.mp.f2, text="厘米",  borderwidth=4,
                  font=self.ft).grid(row=4,column=4,padx=5)
        # 3 user inputs
        ttk.Entry(self.mp.f2, textvariable=self.ans_a, width=6, 
                  foreground='red',font=self.ft).grid(row=2,column=3)
        ttk.Entry(self.mp.f2, textvariable=self.ans_b, width=6, 
                  foreground='red',font=self.ft).grid(row=3,column=3)
        ttk.Entry(self.mp.f2, textvariable=self.ans_c, width=6, 
                  foreground='red',font=self.ft).grid(row=4,column=3)

        self.refresh()

    def refresh(self):
        self.trig_a = np.random.randint(1,8)
        self.trig_b = np.random.randint(1,8)
        self.trig_c = np.random.randint(abs(self.trig_a-self.trig_b),(self.trig_a+self.trig_b))
        self.scale  = np.random.randint(4,8)
        self.trig_circ = (self.trig_a + self.trig_b + self.trig_c)*self.scale 
       
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "已知三角形周长为 %d 厘米，三条边 a, b, c 的边长的比为 %d : %d : %d。\n三条边的长度各是多少厘米？（答案精确到整数厘米即可） " % (self.trig_circ, self.trig_a, self.trig_b, self.trig_c),'n')        
        self.ans_a.set("")
        self.ans_b.set("")
        self.ans_c.set("")
        
    def grade(self):

        length_a = self.scale*self.trig_a
        length_b = self.scale*self.trig_b
        length_c = self.scale*self.trig_c

        try:       
            a = int( self.ans_a.get().replace(" ","").split('.')[0] )
            b = int( self.ans_b.get().replace(" ","").split('.')[0] )
            c = int( self.ans_c.get().replace(" ","").split('.')[0] )
        except:
            a = 0
            b = 0
            c = 0

        flag = False
        message = "错误。答案为(%d,%d,%d) " % (length_a,length_b,length_c) 
        if length_a == a and length_b == b and length_c ==c:
           flag = True
           message = "正确。答案为(%d,%d,%d) " % (length_a,length_b,length_c) 
        return [flag, message]

#----------------------------------------
# side length of a rectangle
class RectangleSideLengths(AbstractProblem):
 
    def __init__(self,root,ft):

        self.rect_circ = 0
        self.rect_a    = 0 
        self.rect_b    = 0 
        self.scale     = 1

        self.ans_a = StringVar()
        self.ans_b = StringVar()

        AbstractProblem.__init__(self,root,ft) 

        ttk.Label(self.mp.f2, text="长 a = ", width=8, borderwidth=4,
                  font=self.ft).grid(row=2,column=2,padx=5)
        ttk.Label(self.mp.f2, text="宽 b = ", width=8, borderwidth=4,
                  font=self.ft).grid(row=3,column=2,padx=5)
        ttk.Label(self.mp.f2, text="厘米",  borderwidth=4,
                  font=self.ft).grid(row=2,column=4,padx=5)
        ttk.Label(self.mp.f2, text="厘米",  borderwidth=4,
                  font=self.ft).grid(row=3,column=4,padx=5)
        # 2 user inputs
        ttk.Entry(self.mp.f2, textvariable=self.ans_a, width=6, 
                  foreground='red',font=self.ft).grid(row=2,column=3)
        ttk.Entry(self.mp.f2, textvariable=self.ans_b, width=6, 
                  foreground='red',font=self.ft).grid(row=3,column=3)

        self.refresh()

    def refresh(self):

        self.rect_a = np.random.randint(2,8)
        self.rect_b = np.random.randint(2,8)
        self.scale  = np.random.randint(2,8)
        if self.rect_b == self.rect_a:
           self.rect_b += 2
        self.rect_circ =  (self.rect_a + self.rect_b)*2*self.scale

        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "已知矩形周长为 %d 厘米，长与宽的边长的比为 %d : %d。\n矩形的长与宽各是多少厘米？（答案精确到厘米即可） " % (self.rect_circ, self.rect_a, self.rect_b),'n')        
        self.ans_a.set("")
        self.ans_b.set("")
        
    def grade(self):

        try:       
            a = int(self.ans_a.get().split('.')[0])
            b = int(self.ans_b.get().split('.')[0])
        except:
            a = 0
            b = 0
 
        a_true = self.rect_a*self.scale
        b_true = self.rect_b*self.scale
        flag = False
        message = "错误。答案为 (%d,%d) " % (a_true,b_true) 
        if a == a_true and b == b_true : 
           flag = True
           message = "正确。答案为 (%d,%d) " % (a_true,b_true) 
   
        return [flag, message]

#-------------------------------------------
class DivideTwoFractions(AbstractProblem):
 
    def __init__(self,root,ft):

        AbstractProblem.__init__(self,root,ft)
 
        # two numerators, two denominatrs, and answer entry
        self.n1      = StringVar()
        self.n2      = StringVar()
        self.d1      = StringVar()
        self.d2      = StringVar()
        self.ans     = StringVar()   # user input

        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,textvariable = self.n2,font=self.ft ).grid(column=2,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d2,font=self.ft ).grid(column=2,row=2)
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,text = '\xF7',    font=self.ft     ).grid(column=1,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501'  ,font=self.ft     ).grid(column=2,row=1)
        ttk.Label(self.mp.f2,text = u' = ' ,    font=self.ft     ).grid(column=3,row=1)
        self.e1 = ttk.Entry(self.mp.f2,textvariable=self.ans, font=self.ft, width=10)
        self.e1.grid(row=1,column=4)

        txt1 = " 分数除法练习\n "
        txt2 = "请将结果写成最简单的形式 (比如 2/4 应为 1/2, 3/1 应为 3)。\n "
        txt3 = "请勿使用小数，并用\"／\"隔开分子与分母。"
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", txt1 + txt2 + txt3,'n')        

        self.refresh()

    def refresh(self):
        self.n1.set(np.random.randint(2,12))
        self.n2.set(np.random.randint(2,12))
        self.d1.set(np.random.randint(2,12))
        self.d2.set(np.random.randint(2,12))
        self.ans.set("")
        
    def grade(self):

        nrt   = int(self.n1.get())*int(self.d2.get())
        dnt   = int(self.d1.get())*int(self.n2.get())       
        gcd   = fractions.gcd(nrt, dnt)
        nrt  /= gcd
        dnt  /= gcd
 
        flag = False
        if dnt == 1: # create and assume a wrong answer 1/0 = infinity
           message = "错误（答案应为 %d） " % nrt  
        else:
           message = "错误（答案应为 %d/%d） " % (nrt, dnt)

        ans_n = 1
        ans_d = 0
        # need deal with possible bizzare input by the user
        frac  = self.ans.get().replace(" ","").split('/')  # at most one "/" allowed
        if len(frac) == 1:
           if  frac[0].isdigit() :    	# user give an integer
               ans_n = int(frac[0]) 
               ans_d = 1         
        elif len(frac) == 2:
           if  frac[0].isdigit() and frac[1].isdigit():
               ans_n = int(frac[0])  
               ans_d = int(frac[1])           
        if nrt == ans_n and dnt == ans_d:
            message = "正确"
            flag = True

        return [flag, message]


#-------------------------------------------
class MultiplyTwoFractions(AbstractProblem):
 
    def __init__(self,root,ft):

        AbstractProblem.__init__(self,root,ft)
 
        # two numerators, two denominatrs, and answer entry
        self.n1      = StringVar()
        self.n2      = StringVar()
        self.d1      = StringVar()
        self.d2      = StringVar()
        self.ans     = StringVar()   # user input

        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,textvariable = self.n2,font=self.ft ).grid(column=2,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d2,font=self.ft ).grid(column=2,row=2)
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,text = u'\u00D7',    font=self.ft     ).grid(column=1,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501'  ,font=self.ft     ).grid(column=2,row=1)
        ttk.Label(self.mp.f2,text = u' = ' ,    font=self.ft     ).grid(column=3,row=1)
        self.e1 = ttk.Entry(self.mp.f2,textvariable=self.ans, font=self.ft, width=10)
        self.e1.grid(row=1,column=4)

        txt1 = " 分数乘法练习\n "
        txt2 = "请将结果写成最简单的形式 (比如 2/4 应为 1/2, 3/1 应为 3)。\n "
        txt3 = "请勿使用小数，并用\"／\"隔开分子与分母。"
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", txt1 + txt2 + txt3,'n')        

        self.refresh()

    def refresh(self):
        self.n1.set(np.random.randint(2,12))
        self.n2.set(np.random.randint(2,12))
        self.d1.set(np.random.randint(2,12))
        self.d2.set(np.random.randint(2,12))
        self.ans.set("")
        
    def grade(self):

        nrt   = int(self.n1.get())*int(self.n2.get())
        dnt   = int(self.d1.get())*int(self.d2.get())       
        gcd   = fractions.gcd(nrt, dnt)
        nrt  /= gcd
        dnt  /= gcd
 
        flag = False
        if dnt == 1: # create and assume a wrong answer 1/0 = infinity
           message = "错误（答案应为 %d） " % nrt  
        else:
           message = "错误（答案应为 %d/%d） " % (nrt, dnt)

        ans_n = 1
        ans_d = 0
        # need deal with possible bizzare input by the user
        frac  = self.ans.get().replace(" ","").split('/')  # at most one "/" allowed
        if len(frac) == 1:
           if  frac[0].isdigit() :    	# user give an integer
               ans_n = int(frac[0]) 
               ans_d = 1         
        elif len(frac) == 2:
           if  frac[0].isdigit() and frac[1].isdigit():
               ans_n = int(frac[0])  
               ans_d = int(frac[1])           
        if nrt == ans_n and dnt == ans_d:
            message = "正确"
            flag = True

        return [flag, message]

#---------------------------------------------
# add/subtract two fractions of same denominator 
class AddSubFracs(AbstractProblem):
 
    def __init__(self,root,ft):

        # two numerators, two denominatrs, and answer entry
        self.n1      = StringVar()
        self.n2      = StringVar()
        self.d1      = StringVar()
        self.d2      = StringVar()
        self.ans     = StringVar()
        self.sgn     = StringVar()

        AbstractProblem.__init__(self,root,ft)

        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,textvariable = self.n2,font=self.ft ).grid(column=2,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d2,font=self.ft ).grid(column=2,row=2)
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,textvariable = self.sgn,font=self.ft).grid(column=1,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501'  ,font=self.ft     ).grid(column=2,row=1)
        ttk.Label(self.mp.f2,text = u' = ' ,    font=self.ft     ).grid(column=3,row=1)
        ttk.Entry(self.mp.f2,textvariable=self.ans, font=self.ft, width=10).grid(column=5,row=1) 

        self.refresh()

    def refresh(self):

        n1 = np.random.randint(32,64)
        n2 = np.random.randint(8,64)
        if n1 < n2:
           n1, n2 = n2, n1
        elif n1 == n2:
           n1 += np.random.randint(10,30)
        d1 = np.random.randint(8,64)
        d2 = d1

        sgn = np.random.randint(0,100) 
        if sgn < 50 :
           self.sgn.set("-")
        else:
           self.sgn.set("+")
       
        self.n1.set(n1)
        self.n2.set(n2)
        self.d1.set(d1)
        self.d2.set(d2)
    
        self.mp.t1.delete("0.0", "end")
        self.mp.t1.insert("0.0", "请完成下面的简单分数加减练习。\n 并请讲结果最简化。",'n')        
        self.ans.set("")
        
    def grade(self):

        nrt   = eval("int(self.n1.get())" + self.sgn.get() + "int(self.n2.get())")
        dnt   = int(self.d1.get())
        gcd   = fractions.gcd(nrt, dnt)
        nrt  /= gcd
        dnt  /= gcd
        if dnt == 1: # create and assume a wrong answer 1/0 = infinity
           message = "错误（答案应为 %d） " % nrt  
        else:
           message = "错误（答案应为 %d/%d） " % (nrt, dnt)
        ans_n = 1
        ans_d = 0
        frac  = self.ans.get().replace(" ","").split('/')
        if len(frac) == 1:
           if  frac[0].isdigit() :    	# user give an integer
               ans_n = int(frac[0]) 
               ans_d = 1         
        elif len(frac) == 2:
           if  frac[0].isdigit() and frac[1].isdigit():
               ans_n = int(frac[0])  
               ans_d = int(frac[1])           
 
        flag = False
        if ans_n == nrt and ans_d == dnt : 
           flag = True
           message = "正确。答案为 (%d/%d) " % (nrt,dnt) 
        return [flag, message]

#------------------------------------------------------
# class simplify fractions (e.g., say 2/4 -> 1/2)
# so just two random number, a/b

class SimplifyFraction(AbstractProblem):

    def __init__(self,root,ft):

        self.l   = 2   # lower-bound for denominator (allow 1 is silly)
        self.h   = 64  # upper-bound for both  (hope this is simple enough)

        AbstractProblem.__init__(self,root,ft) 

        txt1 = "分数简化练习（约分）\n "
        txt2 = "请将结果写成最简单的形式(比如 2/4 应为 1/2, 3/1 应为 3)。\n"
        txt3 = "请勿使用小数，并用／隔开分子与分母。"
        self.mp.t1.insert("0.0",txt1+txt2+txt3,'n')
        self.mp.t1.configure(state="disabled")

        # numerator, denominatr, and answer entry
        self.n1      = StringVar()
        self.d1      = StringVar()
        self.ans     = StringVar()

        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,text = u' = ' ,    font=self.ft     ).grid(column=1,row=1)
        ttk.Entry(self.mp.f2,textvariable=self.ans,font=self.ft, width=10).grid(column=2,row=1) 

        self.refresh()

    def rand(self):  	# upper bound is the same self.h
        return np.random.randint(self.l,self.h)

    def refresh(self):

        self.n1.set(self.rand())
        self.d1.set(self.rand())
        self.ans.set("")

    def grade(self):

        nrt   = int(self.n1.get())
        dnt   = int(self.d1.get())      
        gcd   = fractions.gcd(nrt, dnt)
        nrt  /= gcd
        dnt  /= gcd
        flag  = False
        if dnt == 1: # create and assume a wrong answer 1/0 = infinity
           message = "错误（答案应为 %d） " % nrt  
        else:
           message = "错误（答案应为 %d/%d） " % (nrt, dnt)
        ans_n = 1
        ans_d = 0
        # need deal with possible bizzare input by the user
        frac = self.ans.get().replace(" ","").split('/')  # at most one "/" allowed
        if len(frac) == 1:
           if  frac[0].isdigit() :    	# user give an integer
               ans_n = int(frac[0]) 
               ans_d = 1         
        elif len(frac) == 2:
           if  frac[0].isdigit() and frac[1].isdigit():
               ans_n = int(frac[0])  
               ans_d = int(frac[1])           
        if nrt == ans_n and dnt == ans_d:
            flag = True
            message = "正确"
        return [flag, message]  

#-----------------------------------------------
# class find the lcd of two fractions
class LeastCommonDenominator(AbstractProblem):

    def __init__(self,root,ft):

        self.l1  = 1   # lower-bound for numerator
        self.l2  = 2   # lower-bound for denominator (allow 1 is silly)
        self.h   = 16  # upper-bound for both  (hope this is simple enough)

        AbstractProblem.__init__(self,root,ft) 

        txt1 = "最小公分母练习 \n"
        txt2 = "请写出下面两个分数的最小公分母\n"
        self.mp.t1.insert("0.0",txt1+txt2,'n')
        self.mp.t1.configure(state="disabled")

        # two numerators, two denominatrs, and answer entry
        self.n1      = StringVar()
        self.n2      = StringVar()
        self.d1      = StringVar()
        self.d2      = StringVar()
        self.ans     = StringVar()

        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,textvariable = self.n2,font=self.ft ).grid(column=2,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d2,font=self.ft ).grid(column=2,row=2)
        ttk.Label(self.mp.f2,text = u' 与 ',  font=self.ft       ).grid(column=1,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501'  ,font=self.ft     ).grid(column=2,row=1)
        ttk.Label(self.mp.f2,text = u' 的最小公分母为 ' ,    font=self.ft     ).grid(column=3,row=1)
        ttk.Entry(self.mp.f2,textvariable=self.ans, font=self.ft, width=10).grid(column=5,row=1) 

        self.refresh()
    
    def rand(self,lower):  	# upper bound is the same self.h
        return np.random.randint(lower,self.h)

    def refresh(self):

        self.n1.set(self.rand(self.l1))
        self.n2.set(self.rand(self.l1))
        self.d1.set(self.rand(self.l2))
        self.d2.set(self.rand(self.l2))
        self.ans.set("")

    def grade(self):

        dnt1  = int(self.d1.get())
        dnt2  = int(self.d2.get())       
        gcd   = fractions.gcd(dnt1,dnt2)
        lcm   = dnt1*dnt2//gcd 

        flag    = False
        message = "错误（答案应为 %d） " % lcm  
        frac = self.ans.get().replace(" ","")   # extract user input
        if frac.isdigit() :    	# user give an integer
           if int(frac) == lcm: 
               flag    = True
               message = "正确"
        return [flag, message]  

#--------------------------------------------------------
# a/b = c/d, find c
class EqualFractions(AbstractProblem):

    def __init__(self,root,ft):

        self.l2  = 2   # lower-bound for denominator (allow 1 is silly)
        self.h   = 12  # upper-bound for both  (hope this is simple enough)

        AbstractProblem.__init__(self,root,ft) 

        txt1 = " 等价分数练习\n "
        txt2 = "请将漏掉的数字补上，以使等式成立 "
        txt3 = "(比如 2/4 ＝ a/2, 此处 a 应等于 1)。\n "
        self.mp.t1.insert("0.0",txt1+txt2+txt3,'n')
        self.mp.t1.configure(state="disabled")

        # two numerators, two denominatrs, and answer entry
        self.n1      = StringVar()
        self.n2      = StringVar()
        self.d1      = StringVar()
        self.d2      = StringVar()
        self.ans     = StringVar()
 
        # construct the math frame using labels and answer entry
        ttk.Label(self.mp.f2,textvariable = self.n1,font=self.ft ).grid(column=0,row=0)
        ttk.Label(self.mp.f2,textvariable = self.d1,font=self.ft ).grid(column=0,row=2)
        ttk.Label(self.mp.f2,textvariable = self.n2,font=self.ft ).grid(column=2,row=0)
        ttk.Entry(self.mp.f2,textvariable=self.ans, font=self.ft, width=4).grid(column=2,row=2) 
        ttk.Label(self.mp.f2,text = u'\u2501',  font=self.ft     ).grid(column=0,row=1)
        ttk.Label(self.mp.f2,text = u' = ',     font=self.ft     ).grid(column=1,row=1)
        ttk.Label(self.mp.f2,text = u'\u2501'  ,font=self.ft     ).grid(column=2,row=1)

        self.refresh()

    def refresh(self):

        r3 = np.random.randint(self.l2,self.h,3)
        self.n1.set(r3[0])
        self.d1.set(r3[1])
        self.n2.set(r3[0]*r3[2])
        self.d2.set(r3[1]*r3[2])       
        self.ans.set("")

    def grade(self):

        dnt     = int(self.d2.get())
        flag    = False
        message = "错误（答案应为 %d） " % dnt  
        frac    = self.ans.get().replace(" ","")
        if frac.isdigit(): 
            ans_d = int(frac)           
            if dnt == ans_d:
               flag  = True
               message = "正确"
        return [flag, message]  

